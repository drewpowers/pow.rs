import { h, Component } from 'preact';
import style from './style.css';

const Header = () => (
  <header className={style.header}>
    <h1>Drew Powers</h1>
  </header>
);

export default Header;
